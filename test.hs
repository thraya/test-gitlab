module Main where                                                                                                                   
                                                                                                                                    
import System.IO                                                                                                                    
                                                                                                                                    
main :: IO ()                                                                                                                       
main = do                                                                                                                           
    putStrLn "Welcome to the Hello World program!"                                                                                  
    putStr "What's your name? "                                                                                                     
    hFlush stdout                                                                                                                   
    name <- getLine                                                                                                                 
    putStrLn $ "Hello, " ++ name ++ "!"